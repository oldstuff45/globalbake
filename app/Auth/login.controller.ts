/**
 * Created by lentisas on 8/12/15.
 */
/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import {Authenticate} from '../auth/auth.service'
import {UserService} from '../User/user.service'

interface ILoginParams{
    username: string;
    password: string;
}

interface ILoginCtrl{
    doLogin(params: ILoginParams) : void;
}

class LoginCtrl implements ILoginCtrl{
    State: angular.ui.IStateService;
    constructor(){
        console.log("Login Here")
    }

    doLogin (data: ILoginParams): void{
        let user = new UserService();
        let auth = new Authenticate();
        user.clearUser()
        auth.login(data.username, data.password).then(() => {
            this.State.go('home')
        })
        .catch(res => {
            
        })
        //Todo: Pass Login Params to AuthService
    }
}

export {LoginCtrl}
