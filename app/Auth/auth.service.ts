/**
 * Created by lentisas on 8/12/15.
 */
/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import {UserService} from '../user/user.service'
import {SsiAdapter} from '../services/ssi_adapter'

interface IAuth {
    isLoggedIn(): boolean;
    login(username: string, password: string): angular.IPromise<any>;
    checkLogin(): void;
    logout(): void;
}

class Authenticate implements IAuth {
    Store: angular.localForage.ILocalForageService
    State: angular.ui.IStateService;
    User: UserService
    Adapter: SsiAdapter
    $q: angular.IQService
    
    constructor() {
        
    }
    
    login(username:string, password:string): angular.IPromise<any>{
        let defer: angular.IDeferred<any> =  this.$q.defer();
        this.Adapter.userLogin(username,password).then((response: any) => {
            if(response.data.status){
                let res = response.data;
                //TODO: Get right reponse data structure
                this.User.setUser({
                    oid:res.oid,
                    token:res.token,
                    name:res.rBKdebtor.name
                });
                //TODO: Make login ready for Sales Agent
                this.User.setContactInfo({
                    email:res.rBKcreditorSale.name,                    
                    phone:res.rBKcreditorSale.email
                }); 
                defer.resolve();
            }else{
                defer.reject(response.message);
            }
        },(res: any) => {
           defer.reject(res);
        });
        return defer.promise;
    }

    isLoggedIn(): boolean {
        return this.User.currentUser.oid ? true : false;
    }

    checkLogin(): void {
        if (!this.isLoggedIn()) {
            this.logout();
        }
    }

    logout(): void {
        this.User.clearUser().then(() => {
                this.Store.clear();
                this.State.go('/login');
            },(res: any) => {
            //TODO: Handle logout error better
            console.log("Logout Error", res);
        })
    }
}

export {Authenticate}
