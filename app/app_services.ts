/// <reference path="../typings/angularjs/angular.d.ts" />

import {Authenticate} from './Auth/auth.service';
import {UserService} from './User/user.service';

let AppServices = angular.module("appServices")
	.service(Authenticate)
	.service(UserService);

export {AppServices}
