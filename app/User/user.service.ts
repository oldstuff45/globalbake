/// <reference path="../../typings/tsd.d.ts" />
'use strict';

import {User}  from "../services/schema"

interface IUser {
  currentUser: ICurrentUser;
  contactInfo: ICreditorInfo;
  getToken(): string;
  clearUser(): ng.IPromise<any>;
  setUser(user: ICurrentUser): void; // May need to return a promise instead
  getUser(): Object;
  setContactInfo(info: ICreditorInfo): void; // May need to return a promise instead
}

interface ICurrentUser{
  oid?: string;
  name?: string;
  token?: string;
}

interface ICreditorInfo{
  email:string;
  phone:string;
}

class UserService implements IUser {
  Store: angular.localForage.ILocalForageService;
  currentUser: ICurrentUser;
  $q: angular.IQService;
  contactInfo: ICreditorInfo;
  constructor() { }

  setUser(user: ICurrentUser):void {
    this.Store.setItem("CurrentUser",user).then(() => {
      this.Store.getItem("CurrentUser").then((res) => {
        this.currentUser = res;
      });
    });
  }

  clearUser(): angular.IPromise<any> {
    let defer: angular.IDeferred<any> = this.$q.defer();
    this.Store.removeItem(["CurrentUser","ContactInfo"]).then(() => {
      this.currentUser = {};
      defer.resolve();
    }).catch(reason =>{
      defer.reject(reason);
    })
    return defer.promise;
  }
  
  //May not be needed if not other processing is required on the user
  getUser():ICurrentUser {
    return this.currentUser;
  }
  
  setContactInfo(Info: ICreditorInfo): void{
    //TODO: Get the right info sorted out
    this.Store.setItem("ContactInfo",Info).then(() => {
      this.Store.getItem("ContactInfo").then((res) => {
        this.contactInfo = res;
      })
    })
  }

  getToken() {
    return this.currentUser.token;
  }
}

export {UserService}
