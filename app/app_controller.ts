import { LoginCtrl } from "./Auth/login.controller";
import { UserCtrl } from "./User/user.controller";


let AppControllers = angular.module('appControllers', [])
	.controller('AuthCtrl', LoginCtrl)
	.controller('UserCtrl', UserCtrl);

export {AppControllers}
