/// <reference path="../typings/tsd.d.ts" />

// import {AppControllers} from "./app_controller"
// import {AppServices} from "./app_services"

import { LoginCtrl } from "./Auth/login.controller";
import { UserCtrl } from "./User/user.controller";
import { MainCtrl }  from "./controllers/main";

import { Authenticate } from './Auth/auth.service';
import { UserService } from './User/user.service';

import {AppRoutes}  from './app_routes';

var app = angular.module("globalbake", ['ui.router','LocalForageModule'])
//.service("app.services.Authenticate", Authenticate)
//.service("app.services.UserService", UserService)
	.controller("app.controllers.MainController", MainCtrl)
	.controller("app.controllers.LoginCtrl", LoginCtrl);
//.controller("app.controller.UserController", UserCtrl);
	
app.config(AppRoutes);
app.config(['$localForageProvider',(provider: angular.localForage.ILocalForageProvider)=>{
	provider.setNotify(true,true);
}])
app.run(() => {

})

export { app };
