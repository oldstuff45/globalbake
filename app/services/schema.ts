/// <reference path="../../typings/tsd.d.ts" />

class User {
  oid: string;
  name: string;
  password: string;
  token: string;

  constructor() {  }
}

class Order {
  oid: string;
  edition: number;

  constructor(oid: string) {
    this.oid = oid;
  }
}



export { User, Order };



