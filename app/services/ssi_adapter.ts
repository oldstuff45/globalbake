/// <reference path="../../typings/tsd.d.ts" />

interface ISsiRequestObject {
	command: string;
	parameters: Object;
}

interface ISsiAdapter {
	Http: angular.IHttpService;
	RootUrl: string;
	removeFields(obj: any, fields: Array<string>) : Object;
}

class RequestObject implements ISsiRequestObject {
	command: string;
	parameters: Object;

	constructor(command: string, params: Object) {
		this.command = command;
		this.parameters = params;
	}

}

class SsiAdapter implements ISsiAdapter {
	Http: angular.IHttpService;
	RootUrl: string;

	constructor() {

	}

	//Deletes additional fields added to model
	removeFields(obj: any, fields: Array<string>) {
		//Todo: Compare object with its schema and remove all properties which are not defined in the schema.
		angular.forEach(fields, function(field) {
			delete obj[field];
		});
		return obj;
	}

	userLogin(username: string, password: string) {
		let valRequest = {
            "command":"ValidateUser",
            "parameters": {
                "class": "BKwebUser",
                "password": password,
                "userid": username,
                "properties": {
                    "rBKwebUserLink": {
                        "rBKdebtor": {
                            "name": "",
                            "rBKcreditorSales": {
                                "name": "",
                                "email": ""
                            }
                        }
                    }
                }
            }
		}
        return this.Http.post(this.RootUrl,valRequest);
	}

	//Fetch data from backend.
	query(klass: string, properties: Object, filter: Object) {
        
	}

	//Send data to backend for create
	create(klass: string, params: Object, fieldsToIgnore: Array<string>) {

	}

	//Send data to backend for update
	update(klass: string, params: Object, fieldsToIgnore: Array<string>) {

	}

	//Create request data with required parameters [command, class, token]
	makeRequestObject(command: string, klass: string) {
		var params = {
			"parameters": {
				"class": klass
			}
		}
		return new RequestObject(command, params);
	}
}

export {SsiAdapter};
