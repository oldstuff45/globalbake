/// <reference path="../typings/tsd.d.ts" />

let AppRoutes = ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
    $stateProvider
        .state('home', {
            url: '/',
            template: require('./views/main.html'),
            controller: 'app.controllers.MainController',
            controllerAs: 'Mc'
        })
        .state('login', {
            url: '/login',
            template: require('./auth/login.html'),
            controller: 'app.controllers.LoginCtrl',
            controllerAs: 'loginVm'
        })
        
        $urlRouterProvider.otherwise('/');
}

export {AppRoutes}
